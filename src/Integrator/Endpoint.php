<?php

namespace ZTIS\Integrator;

interface Endpoint
{
	public function fetch($query, $location = null);
	public function isEnabled();
}
