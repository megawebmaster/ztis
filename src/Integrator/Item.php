<?php

namespace ZTIS\Integrator;

class Item implements \JsonSerializable
{
	/** @var string */
	private $id;
	/** @var string */
	private $source;
	/** @var string */
	private $type;
	/** @var string */
	private $message;
	/** @var string */
	private $author;
	/** @var int */
	private $time;
	/** @var string[] */
	private $tags = [];
	/** @var string[] */
	private $media = [];

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string[]
	 */
	public function getMedia()
	{
		return $this->media;
	}

	/**
	 * @param string[] $media
	 */
	public function setMedia(array $media)
	{
		$this->media = $media;
	}

	/**
	 * @return string
	 */
	public function getMessage()
	{
		return $this->message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage($message)
	{
		$this->message = $message;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getSource()
	{
		return $this->source;
	}

	/**
	 * @param string $source
	 */
	public function setSource($source)
	{
		$this->source = $source;
	}

	/**
	 * @return \string[]
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * @param \string[] $tags
	 */
	public function setTags($tags)
	{
		$this->tags = $tags;
	}

	/**
	 * @return string
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @param string $author
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
	}

	/**
	 * @return int
	 */
	public function getTime()
	{
		return $this->time;
	}

	/**
	 * @param int $time
	 */
	public function setTime($time)
	{
		$this->time = $time;
	}

	/**
	 * (PHP 5 &gt;= 5.4.0)<br/>
	 * Specify data which should be serialized to JSON
	 *
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 */
	function jsonSerialize()
	{
		return [
			'type' => $this->type,
			'message' => $this->message,
			'author' => $this->author,
			'media' => $this->media,
			'tags' => $this->tags,
			'source' => $this->source,
		];
	}

	public function getItemValue()
	{
		$value = 0;

		foreach ($this->media as $media) {
			if ($media['type'] == 'image') {
				$value += 10;
				break;
			} else if ($media['type'] == 'video') {
				$value += 5;
				break;
			}
		}

		if (!empty($this->message)) {
			$value += 3;
		}

		$value += count($this->tags);

		return $value;
	}
}
