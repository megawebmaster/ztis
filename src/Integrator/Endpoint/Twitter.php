<?php

namespace ZTIS\Integrator\Endpoint;

use ZTIS\Integrator\Endpoint;

class Twitter implements Endpoint
{
	const URL = 'https://api.twitter.com';
	private $key;
	private $secret;
	private $token;

	function __construct($configuration)
	{
		$this->key = $configuration['key'];
		$this->secret = $configuration['secret'];
	}

	public function fetch($query, $location = null)
	{
		$token = $this->getToken();

		$uri = '/1.1/search/tweets.json?q='.$query.'&result_type=popular';

		if ($location !== null) {
			$radius = round($location['radius']/1000.0, 0).'km';
			$uri .= '&geocode='.$location['latitude'].','.$location['longitude'].','.$radius;
		}

		return $this->fetchContents($token, $uri);
	}

	private function getToken()
	{
		if ($this->token !== null) {
			return $this->token;
		}

		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => self::URL.'/oauth2/token',
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => 'grant_type=client_credentials',
			CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
			CURLOPT_USERPWD => urlencode($this->key).':'.urlencode($this->secret),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTPHEADER => [
				'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
			],
		]);

		$response = curl_exec($ch);
		$response = json_decode($response);

		if (!isset($response->access_token)) {
			$errors = array_map(function($item){ return $item->message; }, $response->errors);
			throw new \Exception('Unable to get access token. Twitter response: '.join(', ', $errors));
		}

		$this->token = $response->access_token;
		return $this->token;
	}

	private function fetchContents($token, $uri)
	{
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => self::URL.$uri,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_HTTPHEADER => [
				'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
				'Authorization: Bearer '.$token,
			],
		]);

		$response = curl_exec($ch);
		$response = json_decode($response);

		if (isset($response->errors)) {
			$errors = array_map(function($item){ return $item->message; }, $response->errors);
			throw new \Exception('Unable to get access token. Twitter response: '.join(', ', $errors));
		}

		return $response->statuses;
	}

	public function isEnabled()
	{
		return true;
	}
}
