<?php

namespace ZTIS\Integrator\Endpoint;

use Doctrine\DBAL\Driver\Connection;
use ZTIS\Integrator\Endpoint;

class Instagram implements Endpoint
{
	const URL = 'https://api.instagram.com';

	private $id;
	private $secret;
	private $token;

	/** @var Connection */
	private $connection;

	function __construct($configuration, Connection $connection)
	{
		$this->id = $configuration['id'];
		$this->secret = $configuration['secret'];
		$this->connection = $connection;
	}

	public function fetch($query, $location = null)
	{
		$token = $this->getToken(false);

		$uri = '/v1/media/search?access_token='.$token;

		if ($location !== null) {
			$radius = $location['radius'];
			$uri .= '&lat='.$location['latitude'].'&lng='.$location['longitude'].'&distance='.$radius;
		}

		$contents = $this->fetchContents($token, $uri);

		return array_filter($contents, function($item) use ($query){
			return in_array($query, $item->tags);
		});
	}

	public function getToken($code)
	{
		if ($this->token !== null) {
			return $this->token;
		}

		$stmt = $this->connection->prepare('SELECT config_value FROM configuration WHERE config_name = :name');
		$params = [
			'name' => 'instagram_endpoint',
		];

		if ($stmt->execute($params)) {
			$result = $stmt->fetchColumn();

			if (!empty($result)) {
				$this->token = $result;

				return $this->token;
			}
		}

		if ($code === false) {
			return false;
		}

		$uri = substr($_SERVER['HTTP_REFERER'], 0, strpos($_SERVER['HTTP_REFERER'], '?'));

		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => self::URL.'/oauth/access_token',
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => 'client_id='.$this->id.'&client_secret='.$this->secret.'&grant_type=authorization_code&redirect_uri='.$uri.'&code='.$code,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_FOLLOWLOCATION => true,
		]);

		$response = curl_exec($ch);
		$response = json_decode($response);

		if (!isset($response->access_token)) {
			throw new \Exception('Unable to get access token. Instagram response: '.$response->error_message);
		}

		$this->token = $response->access_token;

		$stmt = $this->connection->prepare('INSERT OR REPLACE INTO configuration (config_name, config_value) VALUES (:name, :value)');
		$stmt->execute([
			'name' => 'instagram_endpoint',
			'value' => $this->token,
		]);

		return $this->token;
	}

	private function fetchContents($token, $uri)
	{
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => self::URL.$uri,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_HTTPHEADER => [
				'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
				'Authorization: Bearer '.$token,
			],
		]);

		$response = curl_exec($ch);
		$response = json_decode($response);

		if ($response->meta->code !== 200) {
			throw new \Exception('Unable to get access token. Instagram response: '.$response->meta->error_message);
		}

		return $response->data;
	}

	public function isEnabled()
	{
		return $this->getToken(false) !== false;
	}
}
