<?php

namespace ZTIS\Integrator\Parser;

use ZTIS\Integrator\Item;
use ZTIS\Integrator\Parser;

class Instagram implements Parser
{
	public function parse($element)
	{
		$item = new Item();
		$item->setId($element->id);
		$item->setType($element->type);
		$item->setMessage($element->caption->text);
		$item->setSource('instagram');
		$item->setTags(array_unique($element->tags));
		$item->setAuthor($element->user->username);
		$item->setTime($element->created_time);

		$media = [
			[
				'type' => 'image',
				'url' => $element->images->standard_resolution->url,
			],
		];
		$item->setMedia($media);

		return $item;
	}
}
