<?php

namespace ZTIS\Integrator\Parser;

use ZTIS\Integrator\Item;
use ZTIS\Integrator\Parser;

class Twitter implements Parser
{
	public function parse($element)
	{
		$item = new Item();
		$item->setId($element->id);
		$item->setType($this->getType($element));
		$item->setMessage($element->text);
		$item->setSource('twitter');
		$item->setTime(strtotime($element->created_at));
		$item->setAuthor($element->user->screen_name);
		if (isset($element->entities->hashtags)) {
			$item->setTags($element->entities->hashtags);
		}

		if (isset($element->entities->media)) {
			$media = [];
			foreach ($element->entities->media as $mediaItem) {
				$media[] = $this->parseMedia($mediaItem);
			}

			$item->setMedia($media);
		}

		return $item;
	}

	private function getType($element)
	{
		if (isset($element->entities->media)) {
			return 'media';
		}

		return 'text';
	}

	private function parseMedia($media)
	{
		return [
			'type' => $media->type,
			'url' => $media->media_url,
		];
	}
}
