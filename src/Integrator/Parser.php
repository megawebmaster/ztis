<?php

namespace ZTIS\Integrator;

interface Parser
{
	public function parse($item);
}
