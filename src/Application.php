<?php

namespace ZTIS;

use Cocur\Slugify\Bridge\Silex\SlugifyServiceProvider;
use Silex\ControllerProviderInterface;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Symfony\Component\ClassLoader\Psr4ClassLoader;
use Symfony\Component\HttpFoundation\Request;

class Application extends \Silex\Application
{
	const DELETE = 'delete';
	const GET = 'get';
	const PATCH = 'patch';
	const POST = 'post';
	const PUT = 'put';

	private $configuration;
	private $loader;

	public function __construct(array $configuration)
	{
		parent::__construct([
			'debug' => $configuration['debug'],
			'endpoints' => $configuration['endpoints'],
		]);

		$this->configuration = $configuration;
		$this->loader = new Psr4ClassLoader();
		$this->loader->register();

		$this->register(new SlugifyServiceProvider());
		$this->register(new ServiceControllerServiceProvider());
		$this->register(new DoctrineServiceProvider(), array(
			'db.options' => $configuration['database'],
		));
		$this->register(new ServiceProvider\ControllersProvider());
		$this->register(new ServiceProvider\EndpointsProvider());
		$this->register(new ServiceProvider\ParsersProvider());
		$this->register(new ServiceProvider\ServicesProvider());

		// TODO: Security middleware

		$this->before(function (Request $request) {
			if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
				$data = json_decode($request->getContent(), true);
				$request->request->replace(is_array($data) ? $data : array());
			}
		});

		$this->error(function(\Exception $e, $code) {
			if ($code == 404) {
				return $this->json([
					'success' => false,
					'code' => 404,
					'error' => 'Page not found', // TODO: Translations
				]);
			}

			return $this->json([
				'success' => false,
				'code' => $e->getCode(),
				'error' => $e->getMessage(),
			]);
		});

		Request::enableHttpMethodParameterOverride();

		foreach ($configuration['routes'] as $route => $options) {
			$this->addRoute($route, $options);
		}
	}

	private function addRoute($route, array $options)
	{
		switch ($options['type']) {
			case self::DELETE:
				$this->delete($route, $options['action']);
				break;
			case self::GET:
				$this->get($route, $options['action']);
				break;
			case self::PATCH:
				$this->patch($route, $options['action']);
				break;
			case self::POST:
				$this->post($route, $options['action']);
				break;
			case self::PUT:
				$this->put($route, $options['action']);
				break;
		}
	}
}
