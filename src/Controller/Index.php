<?php

namespace ZTIS\Controller;

use Doctrine\DBAL\Connection;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ZTIS\Integrator;

class Index
{
	/** @var Integrator */
	private $integrator;
	/** @var Connection */
	private $connection;

	function __construct(Integrator $integrator, Connection $connection)
	{
		$this->integrator = $integrator;
		$this->connection = $connection;
	}

	public function index(Request $request)
	{
		$query = $request->query->get('query', null);
		$location = [
			'latitude' => $request->query->get('latitude', null),
			'longitude' => $request->query->get('longitude', null),
			'radius' => $request->query->get('radius', null),
		];

		return new JsonResponse($this->getResults($query, $location));
	}

	public function configure(Request $request)
	{
		$type = $request->query->get('type', null);

		switch ($type) {
			case 'instagram':
				$code = $request->query->get('code', false);
				/** @var Integrator\Endpoint\Instagram $endpoint */
				$endpoint = $this->integrator->getEndpoint('instagram');
				$endpoint->getToken($code);
				return new JsonResponse(true);
		}

		return new JsonResponse(false);
	}

	private function getResults($query, $location)
	{
		$time = time();
		$results = [];

		$stmt = $this->connection->prepare('SELECT results, strftime(\'%s\', saved_at) AS saved_at FROM cache WHERE query = :query AND location = :location');
		$params = [
			'query' => $query,
			'location' => join('|', $location),
		];

		if ($stmt->execute($params)) {
			$data = $stmt->fetch(\PDO::FETCH_ASSOC);
			$results = unserialize(base64_decode($data['results']));
			$time = $data['saved_at'];
		}

		if (empty($results) || $time < (time() - Integrator::RESULT_FRESHNESS)) {
			$results = $this->integrator->fetch($query, $location);

			if (!empty($results)) {
				$stmt = $this->connection->prepare('INSERT INTO cache (query, location, results) VALUES (:query, :location, :results)');
				$stmt->execute([
					'query' => $query,
					'location' => join('|', $location),
					'results' => base64_encode(serialize($results)),
				]);
			}
		}

		return $results;
	}
}
