<?php

namespace ZTIS;

use ZTIS\Integrator\Endpoint;
use ZTIS\Integrator\Item;
use ZTIS\Integrator\Parser;

class Integrator
{
	/** A minute. */
	const SAME_TIME_PERIOD = 60000;
	/** 15 minutes. */
	const RESULT_FRESHNESS = 900000;

	/** @var Endpoint[] */
	private $endpoints = [];
	/** @var Parser[] */
	private $parsers;

	function __construct(array $endpoints, array $parsers)
	{
		$this->endpoints = $endpoints;
		$this->parsers = $parsers;
	}

	public function fetch($query, $location = null)
	{
		$results = [];

		foreach ($this->endpoints as $type => $endpoint) {
			if (!$endpoint->isEnabled()) {
				continue;
			}

			$result = $endpoint->fetch($query, $location);
			$part = [];

			foreach ($result as $element) {
				$part[] = $this->parsers[$type]->parse($element);
			}

			$results = $this->integrate($results, $part);
		}

		usort($results, function($a, $b){
			/** @var Item $a */
			/** @var Item $b */
			$result = $a->getItemValue() - $b->getItemValue();

			if ($result == 0) {
				$result = $a->getTime() - $b->getTime();
			}

			return $result;
		});

		return $results;
	}

	public function getEndpoint($name)
	{
		if (!isset($this->endpoints[$name])) {
			throw new \Exception('Invalid endpoint name: '.$name);
		}

		return $this->endpoints[$name];
	}

	public function getParser($name)
	{
		if (!isset($this->parsers[$name])) {
			throw new \Exception('Invalid parser name: '.$name);
		}

		return $this->parsers[$name];
	}

	/**
	 * @param $results Item[] Current items.
	 * @param $part Item[] New items.
	 * @return Item[] List of merged values.
	 */
	private function integrate($results, $part)
	{
		foreach ($part as $k => $item) {
			foreach ($results as $key => $result) {
				if ($this->itemsEquals($item, $result)) {
					$results[$key] = $item->getItemValue() > $result->getItemValue() ? $item : $result;
					unset($part[$k]);
				}
			}
		}

		return array_merge($results, $part);
	}

	/**
	 * @param $item Item
	 * @param $result Item
	 * @return bool
	 */
	private function itemsEquals($item, $result)
	{
		return
			$item->getAuthor() == $result->getAuthor() &&
			($item->getTime() - $result->getTime()) < self::SAME_TIME_PERIOD;
	}
}
