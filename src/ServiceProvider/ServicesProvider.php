<?php

namespace ZTIS\ServiceProvider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use ZTIS\Integrator;

class ServicesProvider implements ServiceProviderInterface
{
	/**
	 * Registers services on the given app.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 *
	 * @param Application $app
	 */
	public function register(Application $app)
	{
		$app['Integrator'] = function() use ($app) {
			return new Integrator(
				[
					'twitter' => $app['Endpoint.Twitter'],
					'instagram' => $app['Endpoint.Instagram'],
				],
				[
					'twitter' => $app['Parser.Twitter'],
					'instagram' => $app['Parser.Instagram'],
				]
			);
		};
	}

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 *
	 * @param Application $app
	 */
	public function boot(Application $app)
	{
	}
}
