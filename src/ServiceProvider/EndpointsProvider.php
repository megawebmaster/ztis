<?php

namespace ZTIS\ServiceProvider;

use ZTIS\Controller\Index;
use ZTIS\Controller\Install;
use Silex\Application;
use Silex\ServiceProviderInterface;
use ZTIS\Integrator;

class EndpointsProvider implements ServiceProviderInterface
{
	/**
	 * Registers services on the given app.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 *
	 * @param Application $app
	 */
	public function register(Application $app)
	{
		$app['Endpoint.Twitter'] = function() use ($app) {
			return new Integrator\Endpoint\Twitter($app['endpoints']['twitter']);
		};
		$app['Endpoint.Instagram'] = function() use ($app) {
			return new Integrator\Endpoint\Instagram($app['endpoints']['instagram'], $app['db']);
		};
	}

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 *
	 * @param Application $app
	 */
	public function boot(Application $app)
	{
	}
}
