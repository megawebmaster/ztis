<?php

namespace ZTIS\ServiceProvider;

use ZTIS\Controller\Index;
use ZTIS\Controller\Install;
use Silex\Application;
use Silex\ServiceProviderInterface;
use ZTIS\Integrator;

class ControllersProvider implements ServiceProviderInterface
{
	/**
	 * Registers services on the given app.
	 * This method should only be used to configure services and parameters.
	 * It should not get services.
	 *
	 * @param Application $app
	 */
	public function register(Application $app)
	{
		$app['Controllers.Index'] = function() use ($app) {
			return new Index($app['Integrator'], $app['db']);
		};
	}

	/**
	 * Bootstraps the application.
	 * This method is called after all services are registered
	 * and should be used for "dynamic" configuration (whenever
	 * a service must be requested).
	 *
	 * @param Application $app
	 */
	public function boot(Application $app)
	{
	}
}
