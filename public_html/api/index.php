<?php

define('ROOT_DIR', __DIR__.'/../..');
define('PUBLIC_DIR', ROOT_DIR.'/public_html/');

require_once(ROOT_DIR.'/vendor/autoload.php');
$config = require_once(ROOT_DIR.'/config.php');

$app = new \ZTIS\Application($config);
$app->run();
