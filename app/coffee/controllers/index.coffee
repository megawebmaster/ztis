app.controller 'IndexController', ['$scope', '$interval', 'Restangular', 'Configuration',
  ($scope, $interval, Restangular, Configuration) ->
    intervalLoop = false
    setInterval = ->
      $interval(
        ->
          $scope.currentItem += 1
          if $scope.currentItem >= $scope.items.length
            $scope.currentItem = 0
        10000
      )
    $scope.items = []
    $scope.currentItem = 0
    $scope.looping = true
    $scope.show = (item) ->
      $scope.currentItem = item
      $scope.stop()
    $scope.restart = ->
      intervalLoop = setInterval()
      $scope.looping = true
    $scope.stop = ->
      $interval.cancel(intervalLoop)
      $scope.looping = false

    getQueryParams = ->
      query: encodeURIComponent(Configuration.tag)
      latitude: Configuration.position.latitude
      longitude: Configuration.position.longitude
      radius: Configuration.radius

    updateValues = ->
      Restangular.all('').getList(getQueryParams()).then (items) ->
        $scope.items = items
        $interval.cancel(intervalLoop)
        intervalLoop = setInterval()

    Configuration.watch(updateValues)

    updateValues()
]
