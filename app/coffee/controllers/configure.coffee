app.controller 'ConfigurationController', ['$scope', '$timeout', '$location', '$modalInstance',
                                           'Configuration', 'Restangular',
  ($scope, $timeout, $location, $modalInstance, Configuration, Restangular) ->
    instagramUrl = "https://api.instagram.com/oauth/authorize/?client_id="
    instagramUrl = instagramUrl + Configuration.instagram.clientId
    instagramUrl = instagramUrl + "&redirect_uri=#{$location.absUrl()}&response_type=code"

    parameters = $location.search()
    if parameters.code?
      Restangular.one('configuration').get(
        type: 'instagram'
        code: parameters.code
      )

    $scope.hashtag = Configuration.tag
    $scope.instagramUrl = instagramUrl
    $scope.close = ->
      $modalInstance.dismiss('cancel')
    $scope.save = ->
      Configuration.tag = $scope.hashtag
      Configuration.update()
      $modalInstance.close()

    $scope.map =
      center: angular.copy(Configuration.position)
      events:
        tilesloaded: (instance) ->
          google.maps.event.trigger(instance, 'resize')
      zoom: 8
      circle:
        center: angular.copy(Configuration.position)
        radius: Configuration.radius
        fill:
          color: '#08B21F'
          opacity: 0.5
        stroke:
          weight: 2
          color: '#08B21F'
          opacity: 1
        clickable: true
        draggable: true
        editable: true
        visible: true
        events:
          center_changed: ->
            Configuration.position =
              latitude: $scope.map.circle.center.latitude
              longitude: $scope.map.circle.center.longitude
            Configuration.radius = $scope.map.circle.radius
]
