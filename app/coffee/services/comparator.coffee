app.service 'Comparator', [ ->
  obj =
    unique: (oldCollection, newCollection) ->
      newItems = []
      angular.forEach newCollection, (item) ->
        exists = obj.contains(oldCollection, item)
        exists or newItems.push(item)
      oldCollection.concat(newItems)

    contains: (collection, item) ->
      angular.forEach collection, (testedItem) ->
        return true if _.isEqual(item, testedItem)
      false

  obj
]
