app = angular.module 'ztis', ['restangular', 'ui.router', 'ui.bootstrap', 'ngSanitize',
                              'ngAnimate', 'angular-velocity', 'uiGmapgoogle-maps', 'rt.debounce']

app.config(
  ['$locationProvider', '$stateProvider', '$urlRouterProvider', 'RestangularProvider', 'uiGmapGoogleMapApiProvider',
  ($locationProvider, $stateProvider, $urlRouterProvider, RestangularProvider, uiGmapGoogleMapApiProvider) ->
    $urlRouterProvider.otherwise('/')
    $locationProvider.html5Mode(true)
    RestangularProvider.setRequestSuffix('/')
    RestangularProvider.setBaseUrl('/api')

    $stateProvider.state('list',
      url: '/'
      templateUrl: 'partials/index.html'
      controller: 'IndexController'
    )
    $stateProvider.state('list.configuration',
      url: 'configure'
      onEnter: ['$modal', '$state', ($modal, $state) ->
        modalInstance = $modal.open(
          templateUrl: 'partials/configuration.html'
          controller: 'ConfigurationController'
          size: 'lg'
        )
        modalInstance.result.finally(->
          $state.go('list')
        )
      ]
    )

    googleMapsConfig =
      key: 'AIzaSyBNZpjVDDUzv8OSt5KPoSTpyvIXKhPUfJU'
      v: '3.17'
      libraries: 'weather,geometry,visualization'
    uiGmapGoogleMapApiProvider.configure(googleMapsConfig)
  ])

app.service('Configuration', ->
  observers = []

  return {
    tag: 'twitter'
    position:
      latitude: 50.06
      longitude: 19.95
    radius: 500000
    instagram:
      clientId: 'e23a0c20b87d493f875cace263b09101'

    update: ->
      for observer in observers
        observer()
    watch: (observer) -> observers.push(observer)
  }
)
