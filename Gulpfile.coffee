gulp = require('gulp')
bower = require('main-bower-files')
coffee = require('gulp-coffee')
coffeelint = require('gulp-coffeelint')
concat = require('gulp-concat')
es = require('event-stream')
inject = require('gulp-inject')
less = require('gulp-less')
cssmin = require('gulp-cssmin')
check = require('gulp-if')
uglify = require('gulp-uglify')
rimraf = require('gulp-rimraf')
replace = require('gulp-replace')

gulp.task 'styles', ->
  gulp.src 'app/less/*.less'
    .pipe less()
    .pipe cssmin()
    .pipe gulp.dest('public_html/css')

gulp.task 'lint', ->
  gulp.src 'app/coffee/**/*.coffee'
  .pipe coffeelint('coffeelint.json')
  .pipe coffeelint.reporter()
  .pipe coffeelint.reporter('fail')

gulp.task 'coffee', ['lint'], ->
  gulp.src 'app/coffee/**/*.coffee'
  .pipe coffee({bare: true})
#  .pipe uglify()
  .pipe gulp.dest('.tmp/compiled')

gulp.task 'scripts', ['coffee'], ->
  gulp.src '.tmp/compiled/**/*.js'
    .pipe concat('app.min.js')
    .pipe gulp.dest('public_html/js')

gulp.task 'partials', ->
  gulp.src 'app/partials/**/*.html'
    .pipe gulp.dest('public_html/partials')

gulp.task 'html', ['styles', 'scripts', 'partials'], ->
  gulp.src 'app/*.html'
    .pipe inject(es.merge(
      gulp.src(bower(), {read: false}),
      gulp.src([
          'public_html/js/**/*.min.js',
          'public_html/css/**/*.css',
          '!bower/**/*'
        ], {read: false})
    ), {ignorePath: ['app/', 'public_html/']})
    .pipe gulp.dest('public_html/')

gulp.task 'fonts', ->
  gulp.src 'bower/bootstrap/fonts/*'
  .pipe gulp.dest('public_html/fonts')

gulp.task 'clean', ->
  gulp.src ['public_html/css/*', 'public_html/js/*', 'public_html/fonts/*'], {read: false}
  .pipe rimraf()

gulp.task 'watch', ['styles', 'scripts', 'fonts', 'partials'], ->
  gulp.watch ['app/coffee/**/*.coffee'], ['scripts']
  gulp.watch ['app/less/**/*.less'], ['styles']

gulp.task 'default', ['clean', 'fonts', 'html']
