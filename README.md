# Zaawansowane Techniki Integracji Systemów - projekt

## Installation

1. Clone repository
2. Install Node.js, PHP 5.6, SQLite 3
3. Run following commands to install required dependencies:
    * `npm install`
    * `bower install`
    * `composer install`
4. Create database structure using command: `cat schema.sql | sqlite3 database.db`
5. Copy `config.php.sample` to `config.php` and fill in missing sensitive data.
6. Run `gulp` to build application
7. Start PHP built-in server `php -t public_html -S 127.0.0.1:8000`
8. Navigate to `localhost:8000` and see you application!