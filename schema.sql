CREATE TABLE IF NOT EXISTS cache (
  id INTEGER PRIMARY KEY ASC,
  query VARCHAR(255) NOT NULL,
  location VARCHAR(255) NOT NULL,
  results TEXT NOT NULL,
  saved_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS configuration (
  config_name VARCHAR(255) NOT NULL,
  config_value VARCHAR(255) NOT NULL,
  PRIMARY KEY (config_name)
);
